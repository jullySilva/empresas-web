import React from "react";
import "./empresa.css";
import logoWhite from "../img/logo-white.png";
import Search from '../img/search.png';
import { ToDoEnterprise } from '../services/Empresa'

function Empresas() {

    const ListEmpresas = async() => {
        const response = await ToDoEnterprise();
    }


  return (
    <div>
      <header className="menu">
        <img src={logoWhite} className="img-logo" alt="Logo Ioasys" />

        <div className="search">
          <input type="text" className="inpsearch" id="txtBusca" placeholder="Buscar" />
          <img src={Search} id="btnBusca" onClick={ListEmpresas} alt="Buscar" />
        </div>
      </header>
      <section className="container">Clique na busca para iniciar.</section>
    </div>
  );
}

export default Empresas;
