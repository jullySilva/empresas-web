import api from '../api';


const LogarUsuario = async (email, password) => {

  const response = await api.post('users/auth/sign_in/', {email, password});

  return response;

};

export default LogarUsuario;
