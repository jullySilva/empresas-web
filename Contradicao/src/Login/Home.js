import React, { useState } from "react";
import LogarUsuario from '../services/LoginResquest/index'
import { useDispatch } from 'react-redux';
import { isAuthenticated, tokenLogin, idLogin, clientLogin } from '../storage/actions'
import logo from "../logo.png";
import email from "../img/email.svg";
import cadeado from "../img/cadeado.svg";
import "./App.css";

function Home(props) {
  const [emailLogin, setEmailLogin] = useState('');
  const [password, setPassword] = useState('');
  const [logar, setLogar] = useState('');
  

  const ValidarLogin = async (e) => {
    const dispatch = useDispatch();
    e.preventDefault();

    if (!emailLogin || !password) {
      setLogar({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = LogarUsuario(emailLogin, password);

        dispatch(isAuthenticated(true));
        dispatch(tokenLogin(response.data.headers['access-token']));
        dispatch(idLogin(response.data.headers['uid']));
        dispatch(clientLogin(response.data.headers['client']));
        
        props.history.push('/Empresas');
      } catch (err) {
        setLogar({
          error:
            "Houve um problema com o login, verifique suas credenciais!",
        });
      }
    }
  };

  const visiblePassword = () => {
    var input = document.querySelector("#password input");
    input.type = input.type == "password" ? "text" : "password";
  };

  return (
    <div className="Header">
      <form onSubmit={ValidarLogin} className="FormLogin">
        <img src={logo} className="img-logo" alt="Logo Ioasys" />
        <p className="title">BEM-VINDO AO EMPRESAS</p>
        <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
        {logar && <p className="warning">{logar}</p>}
        <div className="input-icon">
          <img src={email}></img>
          <input
            className="inplogin"
            type="email"
            placeholder="Endereço de e-mail"
            onChange={(e) => setEmailLogin(e.target.value )}
          />
        </div>
        <div className="input-icon" id="password">
          <img src={cadeado}></img>
          <input
            className="inplogin"
            type="password"
            placeholder="Senha"
            onChange={(e) => setPassword( e.target.value )}
          />
          <img
            className="passwordIcon"
            src="http://i.stack.imgur.com/H9Sb2.png"
            onClick={visiblePassword}
          />
        </div>
        <button type="submit">Entrar</button>
      </form>
    </div>
  );
}

export default Home;
