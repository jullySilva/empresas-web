import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useSelector } from 'react-redux';

import Login from './Login/Home'
import Empresas from './Empresas/index'
import NotFound from './404'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      true ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={<Login />} />
      <PrivateRoute path="/Empresas" component={<Empresas />} />
      <Route path="*" component={() => <NotFound />} />
    </Switch>
  </BrowserRouter>
);

export default Routes;