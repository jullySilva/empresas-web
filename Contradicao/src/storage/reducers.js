import { TOKEN, UID, CLIENT, VERIFICATION } from "./actions";

const initialState = { 
  token: null,
  uid: null,
  client: null,
  verication: false 
};

function commonRoot(state = initialState, action) {
  switch (action.type) {
    case TOKEN:
      return { ...state, token: action.token };
    case UID:
      return {...state, uid: action.uid};
    case CLIENT:
      return {...state, client: action.client};
    case VERIFICATION:
        return { ...state, verification: action.verication};
    default:
      return state;
  }
}

export default commonRoot;