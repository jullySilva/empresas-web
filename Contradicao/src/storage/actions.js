/* ACTION TYPES */
export const TOKEN = 'TOKEN';
export const UID = 'UID';
export const CLIENT = 'CLIENT';
export const VERIFICATION = 'VERIFICATION';

/* ACTION CREATORS */
export function isAuthenticated(VERIFICATION){
    return { type: VERIFICATION, verification }
}

export function tokenLogin(TOKEN) {
  return { type: TOKEN, token }
}

export function idLogin(UID){
  return { type: UID, uid }
}

export function clientLogin(CLIENT) {
  return { type: CLIENT, client }
}
